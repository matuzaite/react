import React from "react";
import "./NavBar.scss";
import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <div className="navbar">
      <ul className="options">
        <li>
          <NavLink to="/">🏠</NavLink>
        </li>
        <li>
          <NavLink
            className="option"
            activeStyle={{ backgroundColor: "rgb(119, 126, 119)" }}
            to="/postlist"
          >
            Post List
          </NavLink>
        </li>
        <li>
          <NavLink
            className="option"
            activeStyle={{ backgroundColor: "rgb(119, 126, 119)" }}
            to="/todolist"
          >
            To Do List
          </NavLink>
        </li>
        <li>
          <NavLink
            className="option"
            activeStyle={{ backgroundColor: "rgb(119, 126, 119)" }}
            to="userlist"
          >
            User List
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default NavBar;

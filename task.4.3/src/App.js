import "./App.scss";
import NavBar from "./components/NavBar";
import { BrowserRouter, Route } from "react-router-dom";
import Home from "./components/Home";
import PostList from "./components/PostList";
import ToDo from "./components/ToDo";
import UserList from "./components/UserList";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <NavBar />
        <Route path="/" exact component={Home} />
        <Route path="/postlist" exact component={PostList} />
        <Route path="/todolist" exact component={ToDo} />
        <Route path="/userlist" exact component={UserList} />
      </div>
    </BrowserRouter>
  );
}

export default App;

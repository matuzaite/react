import "./App.scss";
import { useState } from "react";
import ClassApp from "./ClassApp";

function App() {
  const [count, setCount] = useState(0);

  const plusHandle = () => {
    setCount(count + 1);
  };

  const minusHandle = () => {
    if (count === 0) {
      return;
    } else {
      setCount(count - 1);
    }
  };

  return (
    <>
      <div className="App">
        <h2>Current Count Function</h2>
        <h1>{count}</h1>
        <div>
          <button onClick={plusHandle}>+</button>
          <button onClick={minusHandle}>-</button>
        </div>
      </div>
      <ClassApp />
    </>
  );
}

export default App;

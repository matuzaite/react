import React, { Component } from "react";
import "./App.scss";

export class ClassApp extends Component {
  state = { count: 0 };

  plus = () => {
    this.setState({ count: this.state.count + 1 });
  };

  minus = () => {
    if (this.state.count === 0) {
      return;
    } else {
      this.setState({ count: this.state.count - 1 });
    }
  };

  render() {
    return (
      <div className="App">
        <h2>Current Count Class</h2>
        <h1>{this.state.count}</h1>
        <div>
          <button onClick={this.plus}>+</button>
          <button onClick={this.minus}>-</button>
        </div>
      </div>
    );
  }
}

export default ClassApp;

import { createStore, applyMiddleware, combineReducers } from "redux";
import thunk from "redux-thunk";
import postsReducer from "../../modules/Posts/reducers/postsReducer";
import toDosReducer from "../../modules/ToDo/reducers/toDosReducer";
import usersReducer from "../../modules/Users/reducers/usersReducer";

const allReducers = combineReducers({
  posts: postsReducer,
  todos: toDosReducer,
  users: usersReducer,
});

const store = createStore(allReducers, applyMiddleware(thunk));

export default store;

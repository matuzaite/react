import React from "react";
import "./styles.scss";

const Home = () => {
  return <h1 className="home-container">Fetching data with redux</h1>;
};

export default Home;

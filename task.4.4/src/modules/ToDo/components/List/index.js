import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import "./styles.scss";
import ToDo from "../ToDo";
import { fetchToDos } from "../../actions";

const ToDoList = () => {
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todos);

  useEffect(() => {
    dispatch(fetchToDos());
  }, [dispatch]);

  return (
    <div className="todos-container">
      <div className="todos-body">
        <h1>TO DO LIST</h1>
        {/* <div>
          <input type="text" />
          <button>add</button>
        </div> */}
        {todos.map((task) => {
          return <ToDo task={task} key={`${task.id}${task.title}`} />;
        })}
      </div>
    </div>
  );
};

export default ToDoList;

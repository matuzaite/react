import React from "react";
import "./styles.scss";

const ToDo = ({ task }) => {
  return (
    <div className="todo-container">
      <ul className="todo-list">
        {task.completed === true ? (
          <li style={{ textDecoration: "line-through" }}>{task.title}</li>
        ) : (
          <li>{task.title}</li>
        )}
      </ul>
    </div>
  );
};

export default ToDo;
